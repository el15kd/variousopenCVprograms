#include <iostream>
#include <opencv2/highgui/highgui.hpp>
using namespace std;
using namespace cv;
int main(int argc, const char**argv) {
	setBreakOnError(true);
	Mat original = imread("C:/Users/KonstantinDragostino/Desktop24129469_790321721140458_7406415558665196549_n", CV_LOAD_IMAGE_UNCHANGED);
	Mat brighter = original + Scalar(80, 80, 80);
	Mat darker = original - Scalar(80, 80, 80);.
	Mat contrast = original - Scalar(80, 80, 80);
	namedWindow("Original", CV_WINDOW_KEEPRATIO);
	namedWindow("Brighter", CV_WINDOW_KEEPRATIO);
	namedWindow("Darker", CV_WINDOW_KEEPRATIO);
	namedWindow("Contrast", CV_WINDOW_KEEPRATIO);
	imshow("Original", original);
	imshow("Brighter", brighter);
	imshow("Darker", darker);
	//increase contrast, make image sharper
	waitKey(30);
	destroyAllWindows();
	return 0;
}
